---
title: "Der Reiter auf der Reichsstraße"
date: 2020-05-05T21:13:47+02:00
draft: false
---
mit Yalande, Alveria, Bregon, Romano und Wakagor

Die Helden trafen sich vor einer Weile und beschlossen gemeinsam zu reisen.
Auf der Reichsstraße nach Havena kam zu beginnender Dämmerung Streit um das Nachtlager auf, Alveria wollte im Gegensatz zum Rest in den Bäumen übernachten, obwohl Havena nur noch wenige Meilen entfernt lag.
Währenddessen wurden sie von einem eiligen Reiter überrascht.
Dieser rief sehr aufgeregt.

Wakagor konnte herausragende Sinnesschärfe beweisen und erkannte sofort, dass der Reiter seinen vergifteten und im sterben liegenden Sohn auf dem Arm hatte.
Währen seiner Beobachtungen blieb er auf der Straße stehen und so musste der Reiter abrupt stoppen.
Er war zunächst unerfreut, doch da die Helden schnell erkannten, dass sie helfen können war er dann sofort sehr dankbar über die Fügung.
Yalande untersuchte den Burschen, welche stark haluzinierte, und konnte ihm mithilfe ihrer Giftheilungsfähigkeiten das Leben retten.
Der Reiter war ein Bauer eines nahgelegenen Hofes und bat die Helden ihn zu begleiten und anschließend die Ursache über die Vergiftung des Jungen aufzuklären.
Romano war es möglich dem Bauern durch geschicktest Verhandeln eine deutlich größere Belohnung zu entlocken.

Am Abend in des Bauer Herwins Hütte genossen die Helden gute Verpflegung und erfragten bei der Bauernfamilie weiteres zu den Umständen des Unheils.
Lediglich Alverias feine Elfennase erlaubte es ihr nicht, den Abend zu genießen.
Sie war dennoch zufrieden mit der Situation, konnte sie nun wie erhofft in einem der umliegenden Bäume ihre Nacht verbringen, während der Rest der Truppe in Herwins Scheune schlief.
Die Helden hatten es durch reichlich zureden und Yalandes beruhigende Fähigkeiten geschafft, den Sohn Herwins, Efferian zu überreden sie zum Ort des Unglücks zu begleiten.
Sie hatten herausgefunden, dass er wohl von einem wild gewordenen Waldschrat beim Schweinehüten überrascht wurde und sein Leben wiederum nur dessen Verwirrtheit während des Angriffs zu verdanken hatte.
Bregons glänzte mit hervorragendem Wissen zu jener Kreatur.
Er wusste, dass sich Waldschrats sonst ganz und garnicht wie beschrieben verhielten.
Weiterhin konnte er der Gruppe einige wichtige Details zu Schwächen der Schrats liefern, die im späteren Aufeinandertreffen von Nutzen sein sollten.

Am nächsten Morgen führte Efferian die Helden zum Ort des Angriffs.
Im Wald angekommen spürten die Helden eine starke Unruhe und Bedrohung.
Zudem war der Wald durch erstaunlich dichtes Unterholz und einen sehr dichten Nebel schwer begehbar.
Die Spuren lasen sich wie beschrieben.
Die Helden wurden plötzlich überrascht ... von einem überlebenden der Schweine des Bauern.
Die Magiekundigen der Gruppe, Romano, Alveria und Yalande, waren sich einig aus einer bestimmten Richtung eine magische Quelle auszumachen.
Sie vermuteten eine dämonische Wirkung, die sich auf die Bewohner des Waldes gelegt haben konnte, so auch den Waldschrat.

Romanos Kater Nuri stellte erste Nachforschungen an und entdeckte in der Richtung auch den Waldschrat.
Nach kurzer Überlegung und erster strategischer Planung machten sich Yalande und Alveria auf, um den Ort genauer zu untersuchen.
Yalande konnte bestätigen, dass es sich um den Ort des dämonischen Ursprungs handelt, für weitere Untersuchungen musste sie jedoch weiter heran.
So vereinte sich die Gruppe zunächst wieder.

Wakagor schlug zunächst vor zum direkten und mutigen Kampfe zu blasen, nach weiteren strategischem Planen verworf er diese Idee jedoch.
Die Gruppe entschied die Feuerschwäche des Waldschrats zu nutzen.
Der mutige Wakagor nahm sich eine flink improvisierte Fackel aus den Wollsocken von Romano, entzündete sie mit seinem Feuerstein und preschte voran um den Schrat zu vertreiben.
Der Herr des Waldes stand zunächst da wie versteinert, er brauchte einige Zeit um sich zu entscheiden, ergriff dann zum Glücke für alle die Flucht.

Yalande besah sich sofort die dämonische Quelle.
Sie stellte fest, dass sie sie mit einem Exorzismus reinigen konnte - dieser würde unter starken Mühen dennoch 2 Stunden benötigen.
Unterdessen lies Wakagor nach einer kurzen Treibjagt von dem Schrat ab und fand zurück zum Rest der Gruppe.
Wieder vereint bewachten die übrigen Helden die Dämonenquelle und Yalande und hielten Ausschau nach der möglichen Rückkehr des Waldschrats, bewaffnet mit weiteren Fackeln, improvisiert aus dem Umhang Alverias.

Nachdem der Exorzismus zur Hälfte gelungen war wurde die Gruppe von einer Kleingruppe Kultisten überrascht.
Durch herausragende Hiebe, besonders durch Bregon, Wakagor sowieso den Kater Nuri wurden diese jedoch in der Luft zerrissen und lagen in kürzester Zeit in Fetzen auf dem Waldboden.
Lediglich Romano musste einen mit Pfeil gespickten Fuß hinnehmen, zugefügt durch seine eigene Hand.
Den respektlosen Umgang mit den Leichen der beiden musste die Borongeweihte Yalande schweigend ertragen, da sie voll auf den Exorzismus konzentriert war.
Schließlich gelang dieser - die Abenteurer spürten sofort ein Aufatmen in dem kleinen Wald.

Danach nahm sich Yalande noch der beiden Leichen an und bestatte sie nach den Regeln ihrer Kunst.
Sie fanden noch eine Botschaft bei einem der toten Kultisten, der sie über die genauer Umstände aufklärte.
Am Ende brachten sie Efferian zurück zu seinem Bauernhof und erhielt reichtlich Dank und eine großzügige Belohnung.
