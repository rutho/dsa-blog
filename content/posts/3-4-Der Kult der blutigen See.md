---
title: "Der Kult der blutigen See - Erforschung am Turm"
description: "Teil 4"
featured_image: '/images/havena-wappen.jpg'
date: 2020-07-02T18:03:47+02:00
draft: false
---
mit Yalande, Alveria, Xakagor, Romano und Bregon (unterstützt durch Elwene)

Am nächsten Morgen werden sie bereits von der Praiosgeweihten erwartet.
Draußen vor dem Tempel steht sie mit Blick auf das Meer in einer heroischen Pose, der Wind weht durch ihr Haar.
Sie stellt sich vor als Elwene.
Sie entschließen sich direkt zum Turm aufzubrechen.
Auf dem Weg tauschen sie sich aus und stellen sich einander vor.
Elwene ist vor allem davon beeindruckt, dass die Gruppe mit Opagor auch die Alten zu schätzen weiß.
Nach ihrem Marsch erreichen sie etwas abseits der Stadt eine Anhöhe direkt an der Küste der Vorinsel Fischerort.
Auf der Anhöhe befindet sich der gesuchte Turm.
Sie schleichen sich mit bedacht näher heran.
Sie sehen eine einzelne Wache vor dem Eingang postiert.
Der Turm ist wie von Graustein vorhergesagt schwer zu beklettern und durch den Einfluss von Küste und Wetter zusätzlich feucht und moosig.
Die Helden teilen sich ein wenig auf, um den Turm auch von anderen Seiten weiter zu erkunden.
Yalande und Romano können auf ihrem Weg links um den Turm nicht viel herausfinden.
Xakagor und Alveria gehen rechts herum.
Xakagor entdeckt an einem Baum haarähnliche, graue Flechten.
Er kann sie nicht zuordnen, aber er nimmt sie mit.
Als die beiden das Ende der Insel erreichen tut sich ihnen eine tiefe, felsige Kluft auf.
Das Meer befindet sich einige Meter unter ihnen.
Sie können sehen, dass sich in Richtung des Turms einige Löcher befinden, die möglicherweise auch menschengroß sind.
Derweil kommt eine Gruppe auf dem Weg zum Turm vorüber.
Bregon und Elwene sind nah bei ihnen und diese daher genau sehen.
Es kommen vier Kultisten vorüber.
Zwei von ihnen eskorieren eine junge Frau, welche scheinbar nicht freiwillig mit ihnen unterwegs ist.
Die anderen beiden Kultisten tragen ein paar Waren in Kisten.
Nuri kann mit ihrer feinen Nase herausfinden, dass es sich um Lebensmittel handelt.
Es wird keiner der versteckten Abenteuerer entdeckt.

Die Helden beschließen ein wenig mehr Risiko einzusetzen um an Informationen zu gelangen.
Alveria soll die Wache mit ein wenige Betörung ablenken und befragen währen Romano den Turm mit seinen magischen Fähigkeiten erklimmen möchte.
Die Wache ist bereits von weitem als seltsam erkennbar.
Er bohrt in der Nase und an anderen Stellen und identifiziert die ergatterten Schätze ausgiebig.
Diese Situation erschwert Alverias Aufgabe erheblich, sie schafft es allerdings ihre inneren Widerstände zu überwinden.
Besonders da sich die Wache, welche sich als Dönnes vorstellt, sehr leicht täuschen lässt.

Die Zeit der Ablenkung kann Romano für sich nutzen und derweil den Turm erklimmen.
Er schaut in die Fenster, welche sich erst in oberen Stockwerken befinden.
Diese sind jedoch klein und teilweise zugestellt, weshalb er nur ein paar kleine Einblick in die oberen Stockwerke erlangt.

Dönnes ist schockverliebt in Alveria und kauft ihr alles ab.
Sie schmeichelt ihm weiterhin widerwillig, versucht Informationen aus ihm herauszubekommen.

Romano schafft es bis auf den Turm.
Oben findet er eine Statue einer ihm unbekannte Magierin und eine Falltür.
Diese ist jedoch verschlossen.
Er hat durch die Fenster bereits herausgefunden, dass sich in der obersten Etage einige Beschwörer aufhalten, daher ist seine Vorsicht gefragt.

Mit Hilfe seiner Magie kann er seine Arme in den wenige Millimeter dünnen Spalt unter der Tür schieben.
Von dort aus gelingt es ihm den Verschlussmechanismus zu öffnen.
Er öffnet die Falltür mit aller Vorsicht.
Nicht bedacht hat er den Lichteinfall.
Ein heller Strahl fällt in den Raum.
Die Beschwörer sind gerade vertieft in eine Ritual und bemerken zunächst nichts.
Romano entschließt sich den Blick schweifen zu lassen, danach jedoch die Tür wieder zu schließen.

Alveria verschiedet sich unter einem Vorwand wieder von Dönnes.
Sie gibt vor weitere Freunde zu suchen und für den Beitritt zum Kult zu begeistern.
Von der Idee ist Dönnes begeistert - so verabschiedet er sich schweren Herzens.
Alveria kehrt zu den anderen zurück.

Romano klettert in der Zeit wieder den Turm herab und stößt ebenfalls zum Rest der Gruppe.
Als alle wieder vereint sind beschließt Romano seine magische Fähigkeit noch einmal zu nutzen um die Löcher hinter dem Turm zu erkunden.
Er findet zunächste einige nutzlose Öffnungen.
Schließlich hat er jedoch Erfolg.
Er findet einen tiefern Zugang in die Wand.
Ihm kommt reichlich Wasser aus einem Wasserfall entgegen.
Durch einen Zauber kann er jedoch trocken bleiben.
Er findet nach kurzer Kletterei einen kleinen Raum mit einer Tür.

Der versucht die Tür zu öffnen.
Dabei hat er jedoch keinen Erfolg.
Sie ist verschlossen durch ein Vorhängeschloss innen und übersteigt dadurch Romanos Fähigkeiten.
Daher klopft er laut am die Tür.

Nach kurzem warten meldet sich eine weibliche Stimme.
Sie fragt vorsichtig, ob es jemand von Kult sei.
Romano verneint.
Die Stimme stellt sich als Kendra vor.
Sie wird vom Kult als Hausmädchen festgehalten.
Sie bietet Informationen und ihre Hilfe an.
Sie kann bestätigen, dass unter anderem ein Pirat gefangen gehalten wird.
Die Beschreibung passt auf Flinn.

Sie bietet an, die Wachen im Erdgeschoss abzulenken.
Sie vereinbaren einen Zeitraum von zehn Minuten.
Bis dahin sollen sich die Helden um Dönnes, die Wache vor dem Turm kümmern.
