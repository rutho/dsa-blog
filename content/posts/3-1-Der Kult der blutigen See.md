---
title: "[Kurzprotokoll] Der Kult der blutigen See - Die Entführung"
description: "Teil 1"
featured_image: '/images/havena-wappen.jpg'
date: 2020-06-11T18:00:47+02:00
draft: false
---
mit Yalande, Alveria, Xakagor, Romano und Bregon

Zusammenkunft mit romano in den drei reisenden
Putzige Tochter idra spielt wirt
Romano hat tiere in havena verkauft
Krakensilber unklar
Botschaft der kultisten unklar - markantes symbol
Alle ins Bett
Romano will eigenes Zimmer weil er wakagor misstraut
Wakagor schleicht sich über und knackt das schloss
Legt sich neben ihn
Überfall auf Gasthaus - 2 Piraten
Werden von idra geweckt
Die Helden helfen aus
Bregon teilt legendär aus
Wakgaor schleicht heran und kann aus dem Hinterhalt attackieren
Piraten fallen schnell
Tragen Anhänger mit Symbol bei sich
Idra unendlich dankbar
Yalande begräbt die toten
Alle wollen wieder ins Bett
Aber Flinn wurde entführt
Sehen noch die Entführer flüchten
Aber zu schnell weg
Hinterließen Spuren Richtung havena und Warnung auf Pergament
Helden erhalten großen dank vom Hof
Brechen dann auf zur Verfolgung
Treffen unterwegs auf Unfallstelle von kumin
Dieser wird von einem hungrigen berglöwen bedroht
Helden können ihn mit Wurstwaren ablenken
Geretteter Händler kumin nimmt Helden dafür mit nach havena
Muss nur noch wagen fixen
Schnellere Reise möglich mit Erholung
Vor den Stadttor Trennung von kumin - der muss noch zum Zoll
Helden werden angehalten von Gnomon
Verzweifelt - Ehemann entführt
Darf nicht in die Stadt
Helden versuchen Stadtwache zu bequatsxhen - fragen auch nach den Entführern
Der ist zu hohl - erreichen daher nichts
Dürfen aber ohne Gnomon rein
Wiedertreffen mit kumin
Versuchen es noch beim Zoll, aber kein Erfolg
Beschließen sich auf zu kumin zu machen

Bild: https://www.facebook.com/DasSchwarzeAuge/posts/10156769783279063
