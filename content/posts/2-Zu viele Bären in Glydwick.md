---
title: "Zu viele Bären in Glydwick"
date: 2020-05-29T21:55:47+02:00
draft: false
---
mit Yalande, Alveria, Xakagor, Flinn und Bregon

Nach der Aufregung des Ersten Abenteuers teilte sich die Heldengruppe auf. Romano machte sich auf die Tiere, die Sie als Dank von Bauer Efferian erhielten möglichst gewinnbringend zu veräußern.
Der Rest der Truppe machte sich auf nach Havena, um sich die Hafenstadt anzusehen. Der Zwerg Wakagor fühlt sich zu nah an den Ozeanen Deres unwohl, weshalb er die Gruppe verlies und seiner Wege zog. Nach einigen Tagen trafen die übrigen
Helden auf Flinn, den charismatischen Pirat und Xakagor, den diebischen Nachtelf. Die beiden zogen schon länger zusammen durch die Gegend, lernten an einem fröhlichen Tavernenabend die restliche Gruppe kennen und beschlossen, eine Zeit lang
zusammen zu reisen.
Der Wirt der Taverne bot den Helden einen Botendienst an. Bepackt mit drei Fässern feinsten Honigmets brach die Gruppe dann ins Landesinnere nach Glydwick auf. Kurz bevor sie ihr Ziel erreichten vernahm die Gruppe Hilferufe von einer nahe
gelegenen Schafsweide. Die Bäuerinnen Alueh und Enid Blaufuss ersuchten nach Hilfe, um ihre Schafsherde vor dem Angriff eines Schwarbären zu retten.
Die Männer der Gruppe zögerten nicht lang und stürmten los das Biest zu bekämpfen. Während sich Bregon im Kampf wacker schlug, sorgte Xakagor mit seiner speziellen, beidhändigen Dolchtechnik für Verwunderung. Alveria und Yalande hielten
sich indess etwas zurück. Alveria zückte ihre Harfe, um den kämpfenden moralische unterstützung zukommen zu lassen, wobei ihr eine Saite riss und der gegenteilige Effekt eintrat. Verwundert von diesem Schauspiel fing sich Flinn einen
Biss des Bären in seine Schulter ein, welcher ihn für einige Tage begleiten werde. Als harter Seemann machte ihm das jedoch nichts weiter aus. Mit einem gezielten Bogenschuss von Alveria und einigen harten Schwerthieben Bregons ließ sich
das in Rage geratene Ungetum zuletzt bezwingen. Doch irgendetwas stimmte mit dem Bären nicht.
Während Flinn mit Met bepackt die beiden Bäuerinnen in die Stadt begleitete, untersuchte die restliche Gruppe vorsichtig das Schlachtfeld. Etwas magisches trug sich hier zu, doch zu deuten wusste die genauen Vorgänge wohl keiner. Die
Spuren des Bären ließen sich zurückverfolgen in ein nahes Gebüsch, wo sie sich jedoch in Luft aufzulösen schienen. Woher kam dieser Bär? Das galt es wohl nun herauszufinden.
Angekommen in der Taverne lernten die Helden die Städtische Bevölkerung etwas kennen. Yalande versuchte mit ihren Heilkräften Flinn den Seemann etwas zu unterstützen, dieser lehnte jedoch zugunsten eines gelungenen Tavernenabends und einigen
Glücksspiels dankend ab. Während Alveria mit ihrer elfischen Stimme für Unterhaltung in der Taverne sorgte, nutzte Xakagor indess die Chance, die wohlhabende Bevölkerung Glydwicks um das ein oder andere Goldstück zu erleichtern.
Als sich der Abend zu Ende zu neigen schien, begab sich die Heldengruppe auf Einladung der Bäuerinnen in deren große Scheune als Nachtstätte. Die beiden boten Ihnen als Dank für Ihre Hilfe eine Unterkunft und etwas Verpflegung, baten
die Gruppe aber im Gegenzug die seltsamen Vorkommnisse in Glydwick etwas genauer zu untersuchen. Immerhin setzte der Baron für jeden erschlagenen Bären ein Kopfgeld von gar 20 Silbertalern aus. Auch das war Anreiz für die Helden, die
Stadt und seine Bewohner zu schützen.
Als die Nacht anbrach und die Helden sich in ihren Schlaf betteten, blieb Alveria erneut wach und vernahm erneut um Mitternacht eine magische Eingebung. Irgendwas stimmte hier nicht, doch auch sie vermag nicht zu deuten, was es ist.
Am folgenden Tag machte sich die Gruppe auf Nachforschungen anzustellen. Ein Besuch im Krämerladen lies die Gruppe ihr Inventar auffrischen und brachte Xakagor den Coup der Woche ein. Tagesrationen für die ganze Gruppe lies dieser ungesehen
der Augen des Ladenbesitzers verschwinden und sorgte so für frohen Mutes in der Gruppe. Auch Alveria konnte sich über einen neuen Umhang freuen, nachdem ihr alter dem Kampf gegen den Waldschrat zum Opfer fiel.
Während einer Befragung der Bauern am Morgen erfuhren die Helden, dass eine Räubergruppe einige Tage vor ihrer Ankunft in Glydwick aufgespürt wurde. Während die meisten der Schurken erschlagen oder vertrieben worden, konnten die Stadtbüttel
zwei der Übeltäter in Stahlkäfigen vor den Toren der Stadt aufhängen. Als Abschreckung hingen Sie da, damit sich ja kein weiter Unhold getrauen wolle die Fähigkeiten der Stadtbüttel in Frage zu stellen.
Diese Schienen den Helden einen Besuch wert, sodass sie sich auf machten die beiden Gefangenen zu befragen. Nachdem sich Bauern und Stadtbüttel allesamt nicht erklären konnten, was in Glydwick gerade geschieht schien dies der nächste
logische Schritt zu sein.
Die Befragung der Diebe schien zunächst erfolglos. Odwin, einer der Schelme, schien sich mit seinem Schicksal abgefunden zu haben. Bedankte sich jedoch für eine heilende Berührung Yalandes und ein Stück Brot mit einem interessanten Hinweis:
der zweite vermeindliche Dieb, welcher wenige Schritt weiter im zweiten Käfig dem Tod nahe Stand, schein kein Teil der Gaunergruppe zu sein.
Scheu und kraftlos lag dieser in seinem Käfig, kaum fähig ein Wort zu sprechen. Auch hier konnte Yalande mit ihren herausragenden Heilkünsten für den Moment helfen. Brandwin vom Fliederhaag, so der Name des Insassen, entpuppte sich nach
einiger Zeit als Druide, der fälschlicherweise in die Fänge der Stadtbüttel geraten schien. Er erzählte der Gruppe von einem Kampf der Feenherrin Farindel und dem mächtigen roten Wyrm, welcher seit Jahren und Jahrzehnten anhielt.
Beide Seiten dieses Kampfes suchten sich Verbündete unter den Sterblichen, so auch Branwin. Er erzählte von seiner Mission ein altes trollisches Relikt zu beschaffen, dessen unbeschreibliche Macht der Herrin Farindel im Kampf zur
Unterstützung dienen sollte. Als er, geschwächt von dieser Mission, auf dem Weg zurück zu seiner Herrin auf die Diebesbande stieß, ging der Rest recht schnell. Die Stadtbüttel griffen ein, verfolgten die bereits gesuchten Räuber und
nahmen Branwin mit gefangen. Sie nahmen alles Habe von Ihnen und steckten Sie zur Abschreckung in Käfige an der Stadtmauer. Auch das Artefakt, welches nach Yalandes Kenntnissen über die alten Sagen wohl das "Faserscheid" zu sein scheint,
wurde ihm dabei abgenommen.
Geformt wie ein Kiefer eines Tieres, mit Zähnen, welche keine Zähne sind. So beschreibt Branwin das Artefakt. Was es zu bewirken vermag entzog sich jedoch seiner Kenntnis.
Rechtschaffend, wie die Heldentruppe ist, versuchten Sie nach dieser offenbar recht glaubwürdigen Geschichte Branwin zu befreien. Das Käfigschloss knackte Xakagor in wenigen Augenblicken, jedoch war Branwin zu schwach, um aus der Öffnung am
oberen Teil des Käfigs zu klettern. Alveria und Xakagor begaben sich daher mit in den Käfig, um Branwin in die Freiheit zu tragen, erlagen jedoch dessen Gewicht und ihrer körperlichen Untüchtigkeit. Auch die Versuche den Käfig durch Springen
aus der Verankerung zu reißen scheiterten kläglich. Ein neuer Plan musste her.
Während die Männer der Runde die Stellung hielten, machten sich Yalande und Alveria auf, die Stadtwache zu suchen. Nach einiger Zeit fanden sie auch einen der Büttel, welcher wohl der beiden Frauen, vor allem der Elfe, recht angetan zu sein schien.
Nach einiger Zeit der Unterhaltung bestätigte der Büttel den Frauen die Vorkommnisse mit der Räuberbande. Vom Artefakt wusste er jedoch nichts. Lediglich ein Kollege von ihm, welcher bei der Aktion dabei war, erzählte seit diesem Tag
von einem Glücksbringer, welchen er wohl stets bei sich trug. Glaubhaft und mit ihrem Charm spielend, brachten die Frauen den Büttel dazu ein Auge zuzudrücken, sollte der Gefangene am nächsten Tag nichtmehr in seinem Käfig sitzen.
Die Möglichkeit einer Audienz beim Stadtvorsteher wäre wohl erst am nächsten Tag möglich, um auf einen Freispruch Branwins zu pledieren. Auch ob dieser erfolgreich wäre bleibt fraglich.
So beschlossen Yalande und Alveria einen erneuten Rettungsversuch zu unternehmen. Auf ihrem Rückweg zur Gruppe besuchten sie erneut den Krämerladen und erwarben ein stattliches Hanfseil, um Branwin aus dem Käfig zu retten. Wie genau sie
dies anstellen wollten, wussten Sie jedoch selber noch nicht.
Xakagor wurde indess, und durch seinen Alkoholgenuss von Flinns Honigmet ungeduldig und begab sich auf die Suche nach Yalande und Alveria. Es dauerte jedoch nicht lang, bis er sich in der Stadt verlief. Nach längerem Suchen und wohl durch
Zufall begegnete er dem Stadtbüttel, welcher vorher mit Yalande und Alveria sprach. Auch Xakagor schien sich gut mit dem Büttel zu verstehen, weshalb dieser ihm nach seiner Aussage, er gehöre zu den beiden Frauen, bereitwillig von deren
Unterhaltung erzählte. Er schickte ihn zum Krämerladen, da dies das letzte Ziel der Frauen war. Da er die Frauen dort nicht antraf, machte er sich zurück zum Stadttor, wo der Rest der Gruppe bereits ungeduldig wartete.
Ein geeigneter, zweiter Plan zur Rettung des Druiden fehlte der Gruppe jedoch noch. Das Seil im Schlepptau entschied sich die Gruppe letztlich dazu, in der Stadt nach weiteren Informationen zu suchen. Sie befragen die Bevölkerung, wurden aber
aufgrund des aufringlichen Verhaltens, einiger alkoholisierter Gruppenmitglieder von den Bewohnern der Stadt gemieden. Selbst die Stadtwache machte die Gruppe auf sich aufmerksam, die sobald ein Verbot über die Gruppe verhängte, weitere
Menschen zu belästigen.
Der Tag verging nun ohne, dass die Gruppe recht große Fortschritte zu machen schien. Enttäuscht, aber mit einem kleinen Fünkchen Hoffnung machten sie sich daher auf erneut die Taverne zum Fasrigen Hahn zu besuchen, in welcher Sie schon am
Vorabend verweilten. Yalande und Alveria konnten von Ihrem Gespräch mit dem Stadtbüttel noch erfahren, dass sich die besagte Stadtwache mit dem Glücksbringer wohl am Abend in genau dieser Taverne des öfteren niederzulassen schien.
Dort angekommen wartete die Gruppe auf die Tavernenbesucher und stärkte sich in der Zeit mit Speiß und Trank. Yalande erhielt nun auch die Chance Flinns Wunde einmal genauer in Augenschein zu nehmen. Mehr als einen Verbandswechsel vermochte
Sie jedoch nicht zu tun, was Flinn wohl nur in seiner harten, männlichen Art bestätigte. Alveria nutzte die Zeit zur Meditation und bereitete sich bereits auf ihren großen Auftritt am Abend in der Taverne vor.
Zu späterer Stunde kamen nun nebst anderer Besucher auch die Stadtwache in das Lokal. Die Gruppe konnte auch relativ schnell den vermeindlichen Besitzer des Glücksbringers ausmachen und entschloss sich, erneute Nachforschungen anzustellen.
Flinn nutzte seine Chance und begab sich mit einigen Humpen Bier an den Tisch der Wache. Misstrauisch aber durstig ließen diese ihn gewähren und schienen schon bald die Gesellschaft und Spendierlaune des Seefahrers zu genießen.
Sie lauschten seinem Seemannsgarn und die Stimmung lockerte sich, bis Alveria ihre Stimme erhob. Erneut ließ sie ihren elfischen Gesang erklingen und zog so die Besucher der Taverne in ihren Bann. Neben einen beträchtlichen Trinkgeld
sang sie sich in die Herzen der Gäste und genoss die Aufmerksamkeit. Doch Xakagor sah genau in dieser Situation seine Chance. Der Büttel, der den Glücksbringer zu haben schien hatte eine Tasche mit sich. Diese klemmte er behutsam zwischen
seine Füße unter dem Tisch. Da es sich bei dem Artefakt um einen tierischen Kiefer handelte, war Xakagor schnell klar, dass der Büttel ihn nicht am Körper tragen konnte.
Angetrunken und von Alverias Gesang in ihren Bann gezogen, schien keiner der Tavernengäste zu bemerken, dass Xakagor blitzschnell und ein Stolpern vortäuschend unter die Tische abtauchte und die Tasche des Büttels entwendete.
Er begab sich flink ins Latrinenhäuschen auf dem Hof der Taverne und plünderte die Tasche. Neben unwichtigen Altagsgegenständen und einem Säckchen Silbermünzen fand er tatsächlich das gesuchte Artefakt und nahm es an sich. Die Tasche
versenkte er im stinkenden Dunkel der Latrine. Nachdem er der Gruppe ein Zeichen gegeben hatte, begaben sich bis auf Flinn alle relativ zügig zurück in die Scheine der Bäuerinnen Alueh und Enid.
Flinn ließ den Abend mit ein paar weiteren Humpen Bier mit den Stattbütteln ausklingen und folgte später.
Die Heldengruppe untersuchte indes das Artefakt, konnten jedoch nichts über dessen Wirkungsweise herausfinden. Yalande konnte die Zähne des Kiefers als alte trollische Raumrunen deuten, der Kiefer selbst scheint alt und verschlissen,
aber von einem große Tier zu stammen. Ungefähr um Mitternacht nahmen sowohl Yalande als auch Alveria einen seltsamen magischen Impuls vom Artefakt ausgehend wahr. Doch alle Umsicht und Untersuchung vermochte ihnen nicht zu deuten,
was diese Magie genau bewirkte.
Die Nacht schritt voran, die Helden betten sich zur Ruhe und beschlossen, den nächsten Tag zu nutzen um mehr über die seltsamen Vorkommnisse rund um Glydwick zu erfahren...
