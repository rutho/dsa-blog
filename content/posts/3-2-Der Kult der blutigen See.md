---
title: "[Kurzprotokoll] Der Kult der blutigen See - Am Markt"
description: "Teil 2"
featured_image: '/images/havena-wappen.jpg'
date: 2020-06-18T18:01:47+02:00
draft: false
---
mit Yalande, Alveria, Xakagor, Romano und Bregon

Bei Kumin empfangen durch seinen Bruder Sordin.
Verbringen die Nacht in deren Hause.
Machen sich am nächsten Morgen nach Empfehlung der beiden Händler auf zum Markt.
Alveria bekommt endlich neue Saiten für die Harfe.
Haben einige seltsame Begegnung beim Versuch Informationen zu bekommen.
Xakagor bestiehlt Sordin.
Romano am Wunschbrunnen.
Viele Hinweise deuten auf Kneipe "Der Krakenkönig".
Ein Spion des Kults fliegt beinah auf, doch die Gruppe wird von einer Kindermeute aufgehalten.
Verfolgungsversuch scheitert
Treffen dabei auf horasische Händlerin.
Diese verspricht ihnen Hilfe.
Machen sich auf zum Hafen zu ihrem Schiff.
Nerviger Schelm.
Sprechen mit teilen der Schiffsbesatzung.
Die wollen am Abend zum Krakenkönig übersetzen und nehmen die Helden mit.
Bis dahin geht Xakagor mit Alveria noch einmal zurück zum Markt um noch ein paar weitere Besorgungen zu machen.
zB Pfeile
Xakagor hat keinen Erfolg auf der Suche nach guten Verkleidungen.
Wird daraufhin von jemandem angesprochen.
Es handelt sich um eine Schmugglerin, sie gehen gemeinsam zu ihrem Versteck.
Sie bietet einige Dinge an.
Xakagor erwirbt falsche Haare und Augenbrauen - der erste Teil seiner Transformation zu Opagor ist vollbracht (dazu später mehr).
Am Abend setzen sie über zu Fischerort und alle gehen gemeinsam zur Kneipe "Krakenkönig".
Dies ist die wohl heruntergekommenste Spielunke ganz Havenas.
Die Seeleute fühlen sich hier überaus wohl - es ist ein Ort der Zusammenkunft der einfachen Leute.
In der Kneipe stellt sich ein Irrtum heraus.
Das gesuchte Symbol des Kults ähnelt dem des Krakenkönig, aber unterscheidet sich.
Die markige Wirtin lädt die Helden auf ein Gespräch bei Bier ein.
Die Helden stimmen dem Gespräch zu, lehnen das Getränk jedoch mehrheitlich ab.
Wirtin klärt auf: Das Symbol stammt von Charyporoth - einer Dämonin und Widersacher von Efferd.
Sie schickt die Gruppe zu Graustein, dem Tempelvorsteher der Efferdkirche in Havena.

