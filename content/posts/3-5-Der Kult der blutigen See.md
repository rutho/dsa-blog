---
title: "Der Kult der blutigen See - Der Turm wird erobert"
description: "Teil 5"
featured_image: '/images/havena-wappen.jpg'
date: 2020-07-09T18:04:47+02:00
draft: true
---
mit Yalande, Alveria, Xakagor, Romano und Bregon (unterstützt durch Elwene)

Romano kehrt also zurück zur Gruppe und berichtet.
Sie gehen sofort los.
Xakagor wird nun zum ersten mal zum vollständigen Opagor und will sich von hinten anschleichen.
Alveria soll für Ablenkung sorgen.
 
Sie gibt vor weitere Anwärter für den Kult gefunden zu haben.
So geht die Gruppe offen auf Dönnes zu.
Unter der Ablenkung kann sich Opagor von hinten unbemerkt nähern.
Er nutzt seine Dolche um ihm übel zuzurichten.
Daraufhin greift Bregon ein und wirft Dönnes zu Boden und fixiert ihn.
Die Helden überlegen kurz was mit ihm zu tun ist.
Ohne lang zu fackeln greift nun Elwene zu ihrem Sonnenzepter und verpasst dem Kultisten einen satten Hieb gegen die Schläfe.

In diesem Moment öffnet sich die Tür zum Turm.
Es ist Kendra.
Sie winkt die Helden herein.
Bregon trägt Dönnes mit sich.
Sie erklärt kurz, dass sie die zwei Kultisten aus dem Erdgeschoss auf ein Bier in den Keller gelockt hat.
Die Gruppe entscheidet sie zu überraschen.
Im Keller angekommen setzen sie den bewusstlosen Dönnes an eine Wand.
Danach stürmen sie in den Nebenraum in dem sich die Kultisten aufhalten.

Xakagor schleicht sich hinter ihnen entlang bevor Bregon und Elwene in den Raum stürzten.
Bregon verfehlt sein Ziel.
Elwene macht sich über ihn lustig und zeigt wie es geht.
Mit einem gewaltigen und perfekt gezielten Schlag kippt sie den saufenden Kultisten mit samt Stuhl zu Boden.
Danach kommt Xakagor aus dem Schatten und gibt ihm den Rest.
Er beobachtet aufmerksam die letzten Lebenszüge des Sterbenden.

Der verbleibende Kultist stellt für die Gruppe keine große Hürde mehr dar.
Auch Bregon findet nun in den Kampf und kann nach einigen Fernkampfattacken der anderen gegen den verbleibenden Kultisten austeilen, welcher dadurch sein Ende findet.

Als nächstes wollen die Helden nach Flinn sehen.
Er befindet sich zwei Räume weiter in einem der Kerker.
Es geht ihm wirklich schlecht und hat sichtbare Spuren Folter und Misshandlung.
Yalande kann ihn ein wenig heilen.
Essen und Trinken lehnt er dankend ab, Kendra hat ihn zumindest damit gut versorgt.
 
Die Abenteuer können die Tür zu seinem Verließ leider nicht öffnen.
Alle Versuche scheitern und sie bemerken, dass es sich um eine Art magische Verriegelung handeln muss.
Daher beschließen sie zunächst den Turm zu erobern um nach dem passenden magischen Schlüssel zu suchen.
Flinn stimmt dem Plan ebenfalls zu.
Es gibt keinen ersichtlichen, anderen Ausweg.
 
Zum einen Hinterhalt zu vermeiden stecken sie Dönnes in eines der freien Verließe.
Bevor sie nach oben aufbrechen wollen sie noch den weiteren Gefangenen aufsuchen.
Laut Kendra handelt es sich um einen Grolm - ein kulturschaffendes, sehr alt wirkendes Wesen von der Größe eines Sechsjährigen.
Dieser soll sehr alchemiebegabt sein und ist vermutlich der Vermisste, dessen Frau die Gruppe vor den Toren Havenas getroffen hat.

Kendra weißt ihn in den Weg zu ihm, möchte sich ihm selbst aber nicht nähern.
Bei ihm angekommen stellen sie fest, dass es tatsächlich der Gesuchte ist.
Er sitzt in keinem Gefängnis, allerdings ist er an einer Fußfessel befestigt, die kein Schloss besitzt.
Xakagor hat sofort eine Idee.
Er reicht ihm seinen Dolch und nickt ihm wissend zu.
Alveria versteht sofort was gemeint ist und geht dazwischen.
Sie nimmt den Dolch an sich und möchte es nicht zulassen.
Daraufhin wird der Grolm wütend.
Er besteht ausdrücklich darauf einen Dolch zu erhalten.
Daher reicht ihm Xakagor einen weiteren seiner Dolche.

Unter schrecklichem Geschrei nimmt er den Dolch und schneidet sich seinen Fuß nach und nach ab.
Das fällt ihm natürlich nicht gerade leicht und er muss gegen seine inneren Widerstände ankämpfen.
Der noch hat er am Ende Erfolg.
Nachdem sein Fuß vollständig abgetrennt ist fällt er sofort in Ohnmacht.
Der nun befreite Grolm wurde anschließend sofort von Yalande behandelt.
Während sie seine Wunde fachmännisch versorgt beobachtet Xakagor aufmerksam das Geschehen.
Nachdem die Wunde ordentlich verbunden ist entschließt sich die Gruppe, die oberen Stockwerke zu erobern.
Aus Mangel an Alternativen beschließen sie, den kleinen Grolm mitzunehmen.
Bregon soll ihn sich auf den Rücken binden.
Nachdem das erledigt ist brechen sie auf.

Da die unteren Etagen frei von Kultisten sind nehmen sie den direkten Weg in das erste Obergeschoss.
In der Etage bietet sich ihnen ein überraschender Anblick.
Hier befindet sich eine Runde Plattform mit vier Säulen.
In der Mitte dieser Plattform steht ein großer Baum.
Etwas abseits steht ein Altar - auf diesem befinden sich vier Steine.

In dem Moment wo der letzte der Abenteurer den Raum betreten hat schließen sich plötzlich alle Zugänge zur Etage.
Sie untersuchen daher den Raum genauer.
In der Plattform befinden sich oben auf den Säulen Fassungen.
Die Steine die etwas auf dem Altar stehen scheinen genau in diese Fassung anzupassen.
Zwischen den Säulen auf der Plattform sind alte Schriftzeichen angebracht.
Yalande und Alveria schaffen es gemeinsam die Schrift zu deuten.

Die ganze Erscheinung und ihre scheinbar magischer Zusammenhang gefallen Xakagor garnicht.
Dieser geht so weit wie möglich auf Abstand zur Einrichtung und begibt sich in eine Ecke der Raumes.
Auf sicheren Abstand zur Plattform.

Sie deuten die Worte: feucht, warm, trocken und kalt.
Die Symbole auf den vier Steinen auf dem Altare erkennen sie als Feuer, Erde, Wasser und Luft.
Nach ein wenig Überlegung können sie die Steine richtig zuordnen: Wasser ist kalt und feucht, Erde ist kalt und trocken, Feuer ist warm und trocken, Luft ist feucht und warm.
Nachdem sie die Steine korrekt platziert haben, beginnen diese leicht zu leuchten.
Sonst geschieht nichts.
Davon verunsichert probieren sie weitere Kombinationen.
Dadurch verschwindet lediglich der Lichtschein.
Sie überlegen und besehen sich die Steine weiter.
Elwene, die am Luftstein steht, muss niesen.
Daraufhin erstrahlt dieser in bunten Farben.
Es scheint, als würde ein leichter Wind um den Stein wehen.
Damit ist die Sache klar.
Alveria gießt etwas von ihrem Wasserschlauch auf den Wasserstein.
Romano nutzt seine Magie um eine kleine Flamme am Feuerstein zu erschaffen.
Yalande nimmt etwas von der Erde des Baumes und gibt sie auf den Erdstein.

Alle Steine springen auf den Kontakt zu ihrem Element an.
In dem Moment wo sich der letzte Stein aktiviert erscheint ein alter Herr im Raum.
Er scheint jedoch nicht aus Fleisch und Blut zu bestehen, sondern wirkt halbdurchlässig und holographisch.
Als die Helden ihn ansprechen wollen beginnt er direkt zu ihnen zu sprechen:

> Kennst du das Bild auf zartem Grunde?
> Es gibt sich selber Licht und Glanz.
> Ein andres ist's zu jeder Stunde,
> Und immer ist es frisch und ganz.
> Im engsten Raum ist's ausgeführet,
> Der kleinste Rahmen fasst es ein;
> Doch alle Größe, die dich rühret,
> Kennst du durch dieses Bild allein.
> Und kannst du den Kristall mir nennen?
> Ihm gleicht an Wert kein Edelstein;
> Er leuchtet, ohne je zu brennen,
> Das ganze Weltall saugt er ein.
> Der Himmel selbst ist abgemalet
> In seinem wundervollen Ring;
> Und doch ist, was er von sich strahlet,
> Noch schöner, als was er empfing.

Gemeinsam überlegen sie und einigen sich auf eine Antwort: Der Mond?
"Diese Antwort ist falsch." - spricht wiederum der Wächter.
Ohne Pause stellt er die nächste Frage:

> Ich sag' dir nicht, was ich dir sage.
> Was ich dir sage, sag' ich dir
> Nur darum, dass du sagest mir,
> Was ich nicht selbst dir sage.

Wieder rätseln die Helden herum.
Yalande schlägt vor, es könnte >Geheimnis< sein. 
Sie können sich jedoch nicht recht entscheiden.
Schließlich sagt Romano: "Ich denke, wir sind einfach zu dumm für Rätsel."
Und das war das Stichwort.
Der Wächter sagt: "Diese Antwort ist korrekt." und verschwindet daraufhin.
Der Raum hellt sich auf und es öffnen sich Zugänge zu den Etagen nach unten und oben.
Die Helden schauen sich ein wenig verwirrt gegenseitig an.
Schulterzuckend und verwundert gehen sie zügig in Richtung der nächsten Etage.

Xakagor schleicht voran und versucht den Raum zunächst zu erkunden.
Wie Romano schon von außen herausfinden konnte handelt es sich um einen Schlafsaal.
Vermutlich liegen einige Kultisten gerade in ihren Betten.
Sie zücken allesamt ihre scharfen Schwerter und Dolche und schleichen sich an dieser heran.
Genau aufeinander abgestimmt lassen sie ihre Waffen niedersaußen lassen die schlafenden Kultisten nie wieder erwachen.
Nach dem geglückten Überraschungsangriff springt Xakagor die Truhe am anderen Ende des Raumes ins Auge.
Er geht näher an diese heran um sie zu untersuchen.
Als er seinen Arm nach ihr austreckt bewegt sich diese plötzlich auf ihn zu.
Sie reißt ihr Maul auf und versucht Xakagor zu attackieren.
Geistesgegenwärtig springt er zurück und rettet sich über einen Schemel auf den Tisch in der Mitte des Raums.
Da über keinerlei kletterfähige Gliedmaßen verfügt kann sie ihm nicht nach dort oben folgen.
Schnell reagieren Bregon und Elwene und verpassen dem aggresiven Trödel jeweils satte Schläge.
Durch ihr beherztes Eingreifen markiert dies das Ende der Truhe.

Xakagor fackelt nicht lange und stürzt sofort wieder vom Tisch herab zur Truhe und durchsucht diese.
Als erstes zieht er eine Schatulle heraus.
In dieser befinden sich reichliche Mengen an edlen Steinen.
Er verbirgt diesen Inhalt vor seinen Mitstreitern, leert die Schatulle in seinen Beutel und präsentiert dem Rest der Gruppe die leere Schatulle.

Als nächstes zieht er einen hölzernen Gegenstand aus der Truhe.
Es handelt sich um eine Harfe - enttäuscht reicht er sie an Alveria weiter.
Diese steht bereits mit leuchtenden Augen neben ihm und drückt die Harfe sofort an ihre Brust und schmust mit ihr.
Alveria spürt, wie die neue Harfe die Artefaktbindung der alten Harfe förmlich aufsaugt und sie diese ebenso verwenden kann.
Die Harfe scheint froh endlich zurück in Elfenhand zu sein und empfindet Alveria als würdige Trägerin.
Sie hat die Harfe sofort als wertvollen und verloren geblaubten Schatz ihres Volks erkannt:
Die Harfe vom Goldregenglanz.
Sie weiht ihre Gruppe über diese ein.
Die seltene Harfe ist ein absolutes Meisterwerk elfischer Handwerkskunst.
Aus dem Totholz eines der heiligen Schwarznussholzbaums der Auelfen gefertig, mit unvergleichbar edlen Verzierungen versehen, soll diese den schönsten Klang aller Harfen Aventurierens haben.
Sie gilt seit Jahrhunderten als verschollen - gestohlen von den Elfen durch böse Menschen.
Einer ihrer bekanntesten Besitzer soll der bekannte Maler Golodion Seemond gewesen sein.

Von dem legenderer Diebstahl soll ein jeder Elf wissen.
Sie soll zusammen mit dem seltenen Dolch vom Goldregenglanz gestohlen wurden sein, welcher von der alten Kultur der Hochelfen aus hellem Mondsilber,auch Madasilber genannt, gefertigt wurde.
Auch der Dolch soll also ein unvergleichbares Werkstück elfischen Handwerks sein.
Dies lässt Xakagor aufhorchen, könnte dies schließlich ein Hinweis auf einen ausergewöhnlichen Fund sein.

Als letztes zieht dieser ein Pergament mit folgendem Inhalt aus der Truhe:

> Lieber Elrod,
> ich hoffe wirklich, dass du dieses überaus Wertvolle Stück sinnvoll verwenden wirst und es dir von großem Nutzen sein wird.
> Es gehört zu den edelsten und wertvollsten Gegenständen die sich je in meinem Besitz befanden. 
> -- gezeichnet: Wolfrad zu Gastenbyl

Damit haben sie also einen Hinweis auf den möglichen Verbleib des Dolches seiner Träume.
Nachdem die Helden die Kiste ihrer feinen Überraschungen beraubt haben, beschließen sie sich wieder auf den Weg nach oben zu machen.
