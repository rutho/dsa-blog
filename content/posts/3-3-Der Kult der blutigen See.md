---
title: "Der Kult der blutigen See - In Fischerort"
description: "Teil 3"
featured_image: '/images/havena-wappen.jpg'
date: 2020-06-25T18:02:47+02:00
draft: false
---
mit Yalande, Alveria, Xakagor, Romano und Bregon

Die Helden machen sich auf den Weg zum Efferdtempel.
Aus der Kneipe heraus ist es bereits Abend und abgesehn vom Lärm in der Kneipe ist es ruhig.
Romano ist etwas unaufmerksam und stolpert.
Beim Blick nach unten stellt sich heraus: Er stolperte über einen blau angelaufenen Zeh.
Daraufhin beginnt sich dieser zu bewegen.
Danach bewegt sich auch die Erde um den Zeh herum.
Ebenso geschieht dies in etwas Entfernung vom Zeh.
Nach einem Moment steigen zwei untote, ertrunkene Seeleute aus dem Boden und attackieren die Gruppe.
Den grandiosen Geweihtenfähigkeiten von Yalande können die Wasserleichen nichts entgegensetzen.
Flankiert und unterstützt von der Gruppe kann sie die Untoten so beinahe im Alleingang bannen.
Nachdem die ersten Untoten zu Staub zerfallen steigen nach beschwörerischem Gemurmel aus einer Seitengasse weitere Figuren aus dem Hafenbecken.
Auch diese sind für die Gruppe keine Hürde.
Ein Teil der Helden setzt sich in Bewegung um dem Ursprung der Stimmt auf den Grund zu gehen.
In einer Sackgasse sehen sie zunächst nichts, sie ist vollgestellt.
Nachdem ein weiterer toter Seemann direkt vor ihnen aufsteht ist es klar:
Hinter einigem Kram versteckt sich der gesuchte Beschwörer.
Allen voran schleicht Xakagor, unterstützt von Romano mit seinem Kater Nuri.
Auch Bregon macht sich auf den Weg.
Yalande und Alveria bleiben auf Abstand.
Hinter den Kisten stellen sie den Beschwörer des Kults, welcher aus der Sackgasse nicht entkommen kann.
Sie richten ihn übel zu und versuchen Informationen zu bekommen.
Er ist allerdings nicht gesprächsbereit.
Nach einigen Andeutungen befreien die Helden ihn von seinem Amulett.
Nach einigen erleideten Schmerzen rückt er doch mit ein paar spärlichen Informationen heraus.
Die Halskette zieht sich im selben Moment sofort eng zusammen.
Das hätte ihn den Kopf gekostet.
Allerdings hört er wiederum auf zu sprechen, da er seinen Kult nicht verraten möchte.
Daraufhin hält sich Xakagor nicht länger zurück und beendet sein Leiden.
Sie lassen ihn liegen und machen sich nun auf den Weg zum Efferdtempel.
Dort spricht gerade Graustein in einer Messer zu seinen Anhängern.
Eine Geweihte bittet sie auf Nachfrage bis zum Ende der Messe zu warten.
Nachdem diese endet spricht sie mit Graustein.
Beide entscheiden sich für eine privaten Audienz mit den Abenteurern.
Währenddessen biegt Xakagor, verkleidet als Opagor zur Spendenbox.
Er leer diese mithilfe seiner Fähigkeiten aus, und wirft den wertlosen Teil der Münzen zurück in die Box.
Wofür er in der Kirche für seine Großzügigkeit gelobt wird.
Dadurch verpasst er es allerdings mit dem Rest der Gruppe dem Beginn der Audienz beizuwohnen.
Ein Vorbeikommen an den Wachen zu den privaten Gemächern von Graustein ist ihm nicht möglich.
Die Gruppe tauscht die gesammelten Informationen mit Graustein aus.
Er erteilt ihnen den klaren Auftrag den Kult aus Havena zu vertreiben.
Am gefundenen Krakensilber ist er sehr interessiert.
Er tauscht es gegen reichlich Gold ein.
Beim erfolgreichen Kampf gegen den Kult verspricht er ihnen außerdem den Turm selbst, welcher sich eigentlich in Besitz der Efferdkirche befindet.
Sie bekommen zusätzliche Hilfe versprochen: Eine mutige Geweihte von der Praioskirche.
Dafür muss die Efferdgeweihte in einen anderen Raum des Tempels und lässt auf ihrem Weg aus den Gemächern von Graustein Opagor eintreten.
Die Gruppe erhält gerade ein paar Gegenstände und Segnungen.
Opagor kommt daher gerade noch rechtzeitig um auch davon zu profitieren.
Da die Helden erst am nächsten Morgen aufbrechen möchten bekommen sie einen Schlafplatz im Tempel angeboten.
Xakagor und Alveria beschließen jedoch nocheinmal aufzubrechen, um den Umhang des getöteten Kultisten zu bergen und zu reinigen.
Diese Kurzmission verläuft ohne Zwischenfälle - danach legen auch sie sich zu den anderen.

